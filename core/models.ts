export interface ILocation {
  lat: number
  lng: number
}

export interface IPostLocationData {
  tripId: number
  location: ILocation
}

export interface IFailure {
  posId: number
  reason: string
}

export interface IPostFailurePOSData {
  tripId: number
  data: IFailure
}

export interface ISuccess {
  posId: number
  invoice: any
}

export interface IPostSuccessPOSData {
  tripId: number
  data: ISuccess
}

export interface IInTransit {
  posId: number
}

export interface IPostInTransitPOSData {
  tripId: number
  data: IInTransit
}

export interface ITripData {
  tripId: number
}
