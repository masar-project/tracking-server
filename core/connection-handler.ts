import { Socket } from 'socket.io'
import { io } from '../index'
import {
  EVENT_DISCONNECT,
  EVENT_POST_LOCATION,
  EVENT_GET_LOCATION,
  EVENT_POST_FAILURE_POS,
  EVENT_GET_FAILURE_POS,
  EVENT_GET_SUCCESS_POS,
  EVENT_POST_SUCCESS_POS,
  EVENT_POST_IN_TRANSIT_POS,
  EVENT_GET_IN_TRANSIT_POS,
  EVENT_GET_START_TRIP,
  EVENT_POST_START_TRIP,
  EVENT_POST_PAUSE_TRIP,
  EVENT_POST_CANCEL_TRIP,
  EVENT_GET_PAUSE_TRIP,
  EVENT_GET_CANCEL_TRIP,
  EVENT_GET_REFRESH,
  EVENT_POST_REFRESH,
  EVENT_GET_COMPLETE_TRIP,
  EVENT_POST_COMPLETE_TRIP,
  EVENT_GET_REFRESH_DASHBOARD,
  EVENT_POST_REFRESH_DASHBOARD,
} from './events'
import {
  IPostFailurePOSData,
  IPostInTransitPOSData,
  IPostLocationData,
  IPostSuccessPOSData,
  ITripData,
} from './models'

const connectionHandler = (socket: Socket) => {
  socket.on(EVENT_POST_LOCATION, onPostLocation)
  socket.on(EVENT_POST_FAILURE_POS, onPostFailurePOS)
  socket.on(EVENT_POST_SUCCESS_POS, onPostSuccessPOS)
  socket.on(EVENT_POST_IN_TRANSIT_POS, onPostInTransitPOS)
  socket.on(EVENT_POST_START_TRIP, onPostStartTrip)
  socket.on(EVENT_POST_PAUSE_TRIP, onPostPauseTrip)
  socket.on(EVENT_POST_CANCEL_TRIP, onPostCancelTrip)
  socket.on(EVENT_POST_COMPLETE_TRIP, onPostCompleteTrip)
  socket.on(EVENT_POST_REFRESH, (_) => onPostRefresh())
  socket.on(EVENT_POST_REFRESH_DASHBOARD, (_) => onPostRefreshDashboard())
  socket.on(EVENT_DISCONNECT, onDisconnect)
}

const onPostLocation = async (data: IPostLocationData) => {
  if (!data) {
    console.error('⚡️[server]: Missing location data')
    return
  }

  io.emit(`${EVENT_GET_LOCATION}-${data.tripId}`, data.location)
}

const onPostFailurePOS = async (data: IPostFailurePOSData) => {
  if (!data) {
    console.error('⚡️[server]: Missing failure data')
    return
  }

  io.emit(`${EVENT_GET_FAILURE_POS}-${data.tripId}`, data.data)
}

const onPostSuccessPOS = async (data: IPostSuccessPOSData) => {
  if (!data) {
    console.error('⚡️[server]: Missing success data')
    return
  }

  io.emit(`${EVENT_GET_SUCCESS_POS}-${data.tripId}`, data.data)
}

const onPostInTransitPOS = async (data: IPostInTransitPOSData) => {
  if (!data) {
    console.error('⚡️[server]: Missing in transit data')
    return
  }

  io.emit(`${EVENT_GET_IN_TRANSIT_POS}-${data.tripId}`, data.data)
}

const onPostStartTrip = async (data: ITripData) => {
  if (!data) {
    console.error('⚡️[server]: Missing start trip data')
    return
  }

  io.emit(`${EVENT_GET_START_TRIP}-${data.tripId}`, data.tripId)
}

const onPostPauseTrip = async (data: ITripData) => {
  if (!data) {
    console.error('⚡️[server]: Missing pause trip data')
    return
  }

  io.emit(`${EVENT_GET_PAUSE_TRIP}-${data.tripId}`, data.tripId)
}

const onPostCancelTrip = async (data: ITripData) => {
  if (!data) {
    console.error('⚡️[server]: Missing cancel trip data')
    return
  }

  io.emit(`${EVENT_GET_CANCEL_TRIP}-${data.tripId}`, data.tripId)
}

const onPostCompleteTrip = async (data: ITripData) => {
  if (!data) {
    console.error('⚡️[server]: Missing complete trip data')
    return
  }

  io.emit(`${EVENT_GET_COMPLETE_TRIP}-${data.tripId}`, data.tripId)
}

const onPostRefresh = async () => {
  io.emit(`${EVENT_GET_REFRESH}`, {})
}

const onPostRefreshDashboard = async () => {
  io.emit(`${EVENT_GET_REFRESH_DASHBOARD}`, {})
}

const onDisconnect = () => {
  console.log(`⚡️[server]: User disconnected`)
}

export default connectionHandler
