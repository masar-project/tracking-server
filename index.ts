import http from 'http'
import express from 'express'
import cors from 'cors'
import { config as configENV } from 'dotenv'
import socketIO from 'socket.io'
import connectionHandler from './core/connection-handler'
import { EVENT_CONNECTION } from './core/events'

// Config ENV
configENV()

const PORT = process.env.PORT || 7000

// Config server.
const app = express()
app.use(cors())
const httpServer = http.createServer(app)
export const io = socketIO(httpServer)

// Handle socket client connections.
io.on(EVENT_CONNECTION, connectionHandler)

// Turn on the server.
httpServer.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${PORT}`)
})
